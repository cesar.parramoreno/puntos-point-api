# Puntos Point API

## Introducción
Para este ejercicio técnico se modelaron usuarios, productos y compras.
Existen relaciones auxiliares que se crearon para manipular las consultas a la base de datos de una forma más eficiente.

La API cuenta con una autenticación hecha en Firebase. Cada request necesita un header de autorización con un token para poder interactuar.

Si bien en un ecommerce real se pueden manejar mayores campos y modelos, se trató de emular la actividad principal llegando a poblar la base de datos con 100.000 registros de compras.

Se utilizaron gemas que permiten mejorar la performance de la aplicación como por ejemplo Bullet, para la eliminacion de n+1 problems entre otras.

## Información sobre la Aplicación
El proyecto fue realizado con ruby version 2.7.0 y como motor de base de datos utilicé PostgreSQL.

# Si desea correr la API...
1) Clonar el repositorio o usar mediante fork.
2) Instalar las dependencias con `bundle install`
3) Correr las migraciones y poblar la base de datos `rails db:migrate db:populate`
4) Iniciar el servidor de rails `rails s`
5) Utilizar la documentación de la API para conocer los parametros que se manejan en cada endpoint.

# API Doc
LINK: https://documenter.getpostman.com/view/16208664/VV53NZWw

# Desarrollo y tests unitarios
La API obtiene información de los productos, que pueden estar categorizados. Existen query params para obtener los productos mediante su categoria, por el nombre de producto e incluso poder obtener los productos más comprados (más populares).

Por el lado de las compras, que se encuentran asociadas a un usuario, también se puede obtener un conjunto de registros de acuerdo a rango de fechas, categorias y productos.

Y por último tambíen contamos con un endpoint que se encarga de procesar las compras de acuerdo a una fecha inicial y una fecha final con el objetivo de poder dar una respuesta que sea compatible para que un Frontend pueda consumir y pueda generar gráficos utilizando granularidad (Por año, por mes y por día).
