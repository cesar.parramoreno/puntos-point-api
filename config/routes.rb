# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      get "granulated_orders", to: "order_charts#index"
      resources :products, only: [:index]
      resources :orders, only: [:index]
    end
  end
end
