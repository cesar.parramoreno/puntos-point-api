# frozen_string_literal: true

module Api
  module V1
    class OrderChartsController < ApplicationController
      helper OrderHelper

      def index
        render index: orders, status: :ok
      end

      private
        def orders
          @orders ||= fetch_orders
        end

        def fetch_orders
          orders = Order.all
          orders = orders.between(starts_at, ends_at) if starts_at && ends_at
          orders
        end

        def starts_at
          params.require(
            [:initial_date]
          )
        end

        def ends_at
          params.require(
            [:final_date]
          )
        end
    end
  end
end
