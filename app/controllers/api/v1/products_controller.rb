# frozen_string_literal: true

module Api
  module V1
    class ProductsController < ApplicationController
      def index
        render index: products, status: :ok
      end

      private
        def products
          @products ||= fetch_products
        end

        def fetch_products
          products = Product.order(created_at: "DESC")
          products = more_popular_products.limit(3) if more_popular.present?
          products = products.product_by_category(category) if category
          products
        end

        def more_popular
          params[:more_popular]
        end

        def category
          params[:category]
        end

        def more_popular_products
          product_ids = ProductOrder.most_popular_products
                                    .collect(&:product_id)

          Product.where(id: product_ids)
        end
    end
  end
end
