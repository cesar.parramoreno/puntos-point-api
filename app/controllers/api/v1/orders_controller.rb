# frozen_string_literal: true

module Api
  module V1
    class OrdersController < ApplicationController
      include Paginable

      def index
        render index: orders, status: :ok
      end

      private
        def orders
          @orders ||= fetch_orders
        end

        def fetch_orders
          orders = Order.order(created_at: "DESC")
                        .includes(:products)
          orders = orders.page(page_number).per_page(page_size) if page_number
          orders = orders.by_date(date) if date
          orders = orders.by_category(category) if category
          orders = orders.by_product_name(product_name) if product_name
          orders = orders.between(starts_at.to_date, ends_at.to_date) if starts_at && ends_at
          orders
        end

        def date
          params[:date]
        end

        def category
          params[:category]
        end

        def product_name
          params[:product_name]
        end

        def starts_at
          params[:start_at]
        end

        def ends_at
          params[:ends_at]
        end
    end
  end
end
