# frozen_string_literal: true

module Authenticable
  extend ActiveSupport::Concern

  included do
    helper_method :current_user

    private
      def authenticate_user!
        render_unauthorized unless current_user.present?
      end

      def current_user
        @current_user ||= find_current_user
      end

      def find_current_user
        return unless firebase_id_token

        firebase_response = FirebaseIdToken::Signature.verify(firebase_id_token)

        return unless firebase_response.present?

        find_or_create_user(firebase_response)
      end

      def find_or_create_user(firebase_response_data)
        begin
          user = User.find_or_initialize_by(firebase_user_id: firebase_response_data["user_id"])

          unless user.persisted?
            user.assign_attributes(
              email: firebase_response_data["email"],
              provider: firebase_response_data["firebase"]["sign_in_provider"]
            )

            user.save!
          end
        rescue ActiveRecord::RecordNotUnique
        end

        user
      end

      def firebase_id_token
        request.headers["Authorization"]
      end
  end
end
