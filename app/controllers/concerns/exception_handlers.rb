# frozen_string_literal: true

module ExceptionHandlers
  extend ActiveSupport::Concern

  included do
    rescue_from ActionController::ParameterMissing, with: :render_parameter_missing
    rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity
    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
    rescue_from ActiveModel::ValidationError, with: :render_model_errors
    rescue_from FirebaseIdToken::Exceptions::NoCertificatesError, with: :no_certificates_error

    private
      def render_unprocessable_entity(exception)
        render json: { errors: exception.record.errors }, status: :unprocessable_entity
      end

      def render_not_found(exception)
        render json: { error: exception.message }, status: :not_found
      end

      def render_parameter_missing(exception)
        render json: { error: exception.message }, status: :bad_request
      end

      def render_errors_for(resource)
        render json: { errors: resource.errors }, status: :unprocessable_entity
      end

      def render_unauthorized
        render json: { error: "Unauthorized" }, status: :unauthorized
      end

      def no_certificates_error
        render json: { error: "There's no certificates in Redis database" }, status: :forbidden
      end
  end
end
