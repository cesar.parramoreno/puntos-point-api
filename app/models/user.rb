# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id               :bigint           not null, primary key
#  email            :string           not null
#  first_name       :string           not null
#  last_name        :string           not null
#  provider         :string           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  firebase_user_id :string           not null
#
# Indexes
#
#  index_users_on_firebase_user_id  (firebase_user_id) UNIQUE
#
class User < ApplicationRecord
  # Associations
  has_many :orders

  # Validations
  validates :first_name,
            :last_name,
            :email,
            :provider,
            presence: true

  validates :firebase_user_id, uniqueness: { case_sensitive: true }
end
