# frozen_string_literal: true

# == Schema Information
#
# Table name: product_orders
#
#  id         :bigint           not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  order_id   :bigint           not null
#  product_id :bigint           not null
#
# Indexes
#
#  index_product_orders_on_order_id    (order_id)
#  index_product_orders_on_product_id  (product_id)
#
# Foreign Keys
#
#  fk_rails_...  (order_id => orders.id)
#  fk_rails_...  (product_id => products.id)
#
class ProductOrder < ApplicationRecord
  # Associations
  belongs_to :product
  belongs_to :order

  # Scopes
  scope :most_popular_products, -> { select("product_id, count(product_id) as count").group(:product_id).order("count desc") }
end
