# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id          :bigint           not null, primary key
#  description :string
#  name        :string           not null
#  price       :decimal(8, 2)    not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Product < ApplicationRecord
  # Associations
  has_many :product_categories
  has_many :categories, through: :product_categories, dependent: :destroy
  has_many :product_orders
  has_many :orders, through: :product_orders, dependent: :destroy

  # Validations
  validates :name,
            :price,
            presence: true

  # Scopes & Callbacks
  scope :product_by_category, -> (product_name) { joins(:categories).merge(Category.by_name(product_name)) }
  scope :by_name, -> (product) { where name: product }
end
