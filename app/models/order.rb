# frozen_string_literal: true

# == Schema Information
#
# Table name: orders
#
#  id             :bigint           not null, primary key
#  bought_at      :date             not null
#  payment_method :string           not null
#  status         :string           default("pending"), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :bigint           not null
#
# Indexes
#
#  index_orders_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Order < ApplicationRecord
  enum status: { pending: "pending", confirmed: "confirmed" }

  # Associations
  belongs_to :user
  has_many :product_orders, dependent: :destroy
  has_many :products, through: :product_orders
  has_many :categories, through: :products

  # Validations
  validates :payment_method,
            :bought_at,
            :status,
            presence: true

  # Scopes & Callbacks
  scope :by_date, -> (date) { where bought_at: date }
  scope :by_category, -> (category) { joins(:categories).merge(Category.by_name(category)) }
  scope :by_product_name, -> (product) { joins(:products).merge(Product.by_name(product)) }
  scope :between, -> (starts_at, ends_at) { where("#{table_name}.bought_at >= ? AND #{table_name}.bought_at <= ? ", starts_at, ends_at).order("bought_at asc") }
  scope :by_year, -> (start_at, end_at) { group_by_year(:bought_at, range: start_at..end_at).count }
  scope :by_month, -> (start_at, end_at) { group_by_month(:bought_at, range: start_at..end_at).count }
  scope :by_day, -> (start_at, end_at) { group_by_day(:bought_at, range: start_at..end_at).count }
end
