# frozen_string_literal: true

json.data do
  json.array! @orders do |order|
    json.(order, :id, :payment_method, :bought_at, :status, :created_at, :updated_at)
    json.products do
      json.array! order.products do |product|
        json.(product, :id, :name, :description, :price)
        json.category do
          json.array! product.categories do |category|
            json.(category, :id, :name)
          end
        end
      end
    end
  end
end

if @orders.respond_to?(:current_page)
  json.partial! "/api/pagination/index"
end
