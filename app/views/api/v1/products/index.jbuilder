# frozen_string_literal: true

json.data do
  json.array! @products.includes(:categories) do |product|
    json.(product, :id, :description, :name, :price, :price, :created_at, :updated_at)
    json.total_sales product.orders.count
    json.categories do
      json.array! product.categories do |category|
        json.(category, :id, :name)
      end
    end
  end
end
