# frozen_string_literal: true

result = get_label_or_value(@orders, params[:initial_date].to_date, params[:final_date].to_date)

json.type chart_type
json.data do
  json.labels result[:labels]
  json.values result[:values]
end
