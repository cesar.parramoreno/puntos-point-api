# frozen_string_literal: true

json.meta do
  json.(@orders, :current_page, :next_page, :previous_page, :total_pages, :total_entries, :per_page)
end
