# frozen_string_literal: true

module OrderHelper
  def chart_type
    "bar"
  end

  def orders_by_year(start, finish)
    Order.by_year(start, finish)
  end

  def orders_by_month(start, finish)
    Order.by_month(start, finish)
  end

  def orders_by_day(start, finish)
    Order.by_day(start, finish)
  end

  def get_label_or_value(orders, start, finish)
    labels = []
    values = []

    parsed_params = JSON.parse(params.to_json)

    if parsed_params["by"] == "year"
      orders_label_by_years = orders_by_year(start, finish)
      orders_label_by_years.each do |order|
        labels << order[0].year
        values << order[1]
      end
    elsif parsed_params["by"] == "month"
      orders_label_by_months = orders_by_month(start, finish)
      orders_label_by_months.each do |order|
        labels << Date::MONTHNAMES[order[0].month] + " #{order[0].year}"
        values << order[1]
      end
    else
      orders_label_by_days = orders_by_day(start, finish)
      orders_label_by_days.each do |order|
        labels << order[0]
        values << order[1]
      end
    end

    {
      labels: labels,
      values: values
    }
  end
end
