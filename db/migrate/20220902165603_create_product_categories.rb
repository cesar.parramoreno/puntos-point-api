class CreateProductCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :product_categories do |t|
      t.belongs_to :product, null: false
      t.belongs_to :category, null: false

      t.timestamps
    end
  end
end
