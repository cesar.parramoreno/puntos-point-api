class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.belongs_to :order
      t.string :name, null: false
      t.string :description
      t.decimal :price, precision: 8, scale: 2, null: false

      t.timestamps
    end
  end
end
