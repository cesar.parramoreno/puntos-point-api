class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :email, null: false
      t.string :provider, null: false
      t.string :firebase_user_id, null: false

      t.timestamps
    end

    add_index :users, :firebase_user_id,     unique: true
  end
end
