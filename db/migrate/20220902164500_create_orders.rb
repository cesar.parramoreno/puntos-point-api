class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.date :shipping_at, null: false
      t.string :payment_method, null: false
      t.string :status, default: "pending", null: false

      t.timestamps
    end
  end
end
