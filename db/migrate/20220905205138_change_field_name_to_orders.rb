class ChangeFieldNameToOrders < ActiveRecord::Migration[6.1]
  def change
    rename_column :orders, :shipping_at, :bought_at
  end
end
