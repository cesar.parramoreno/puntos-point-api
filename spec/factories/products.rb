# frozen_string_literal: true

# == Schema Information
#
# Table name: products
#
#  id          :bigint           not null, primary key
#  description :string
#  name        :string           not null
#  price       :decimal(8, 2)    not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
FactoryBot.define do
  factory :product do
    name { "MyString" }
    description { "MyString" }
    price { "9.99" }

    trait :with_category do
      after(:build) do |product, evaluator|
        product.categories << build(:category)
      end
    end

    trait :with_orders do
      after(:build) do |product, evaluator|
        product.orders << build(:order)
      end
    end
  end
end
