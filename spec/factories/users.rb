# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id               :bigint           not null, primary key
#  email            :string           not null
#  first_name       :string           not null
#  last_name        :string           not null
#  provider         :string           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  firebase_user_id :string           not null
#
# Indexes
#
#  index_users_on_firebase_user_id  (firebase_user_id) UNIQUE
#
FactoryBot.define do
  factory :user do
    first_name { FFaker::Name.first_name }
    last_name { FFaker::Name.last_name }
    email { FFaker::Internet.email }
    provider { "password" }
    sequence(:firebase_user_id) { |n| "firebase_user_id#{n}" }
  end
end
