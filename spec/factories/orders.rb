# frozen_string_literal: true

# == Schema Information
#
# Table name: orders
#
#  id             :bigint           not null, primary key
#  bought_at      :date             not null
#  payment_method :string           not null
#  status         :string           default("pending"), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :bigint           not null
#
# Indexes
#
#  index_orders_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :order do
    user
    bought_at { "2022-09-02" }
    payment_method { "MyString" }
    status { "pending" }

    trait :with_10_products do
      after(:build) do |order, evaluator|
        order.products << create_list(:product, 10)
      end
    end
  end
end
