# frozen_string_literal: true

module RequestHelpers
  module SignInHelper
    def sign_in(user)
      @current_user = user
      @firebase_token = create_token(user)
    end

    def create_token(user)
      payload = {
        "iss": "https://securetoken.google.com/firebase-id-token",
         "name": "Ugly Bob",
         "picture": "https://someurl.com/photo.jpg",
         "aud": "firebase-id-token",
         "auth_time": 1492981192,
         "user_id": user.firebase_user_id,
         "sub": user.firebase_user_id,
         "iat": 1492981200,
         "exp": 33029000017,
         "email": user.email,
         "email_verified": true,
         "firebase": {
            "identities": {
               "google.com": [
                  "1010101010101010101"
               ],
               "email": [
                  "uglybob@emailurl.com"
               ]
            },
            "sign_in_provider": "google.com"
         }
      }

      JWT.encode payload, OpenSSL::PKey::RSA.new(FirebaseIdToken::Testing::Certificates.private_key), "RS256"
    end

    %w[get post put patch delete].each do |http_method|
      # Override request methods to hook login headers
      define_method(http_method) do |path, **args|
        # If there are headers, append them to the request
        if @firebase_token
          args[:headers] ||= {}
          args[:headers] = { "Authorization" => @firebase_token }
        end

        super(path, **args)
      end
    end
  end
end
