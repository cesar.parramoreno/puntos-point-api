# frozen_string_literal: true

# == Schema Information
#
# Table name: categories
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require "rails_helper"

describe Category, type: :model do
  describe "Associations" do
    it { should have_many(:products) }
    it { should have_many(:product_categories) }
  end

  describe "Validations" do
    it { should validate_presence_of(:name) }
  end
end
