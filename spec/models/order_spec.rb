# frozen_string_literal: true

# == Schema Information
#
# Table name: orders
#
#  id             :bigint           not null, primary key
#  bought_at      :date             not null
#  payment_method :string           not null
#  status         :string           default("pending"), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :bigint           not null
#
# Indexes
#
#  index_orders_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
require "rails_helper"

describe Order, type: :model do
  describe "Associations" do
    it { should belong_to(:user) }
    it { should have_many(:products) }
  end

  describe "Validations" do
    it { should validate_presence_of(:payment_method) }
    it { should validate_presence_of(:bought_at) }
    it { should validate_presence_of(:status) }
  end
end
