# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id               :bigint           not null, primary key
#  email            :string           not null
#  first_name       :string           not null
#  last_name        :string           not null
#  provider         :string           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  firebase_user_id :string           not null
#
# Indexes
#
#  index_users_on_firebase_user_id  (firebase_user_id) UNIQUE
#
require "rails_helper"

describe User, type: :model do
  subject(:user) { create(:user) }

  describe "Associations" do
    it { should have_many(:orders) }
  end

  describe "Validations" do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:provider) }
    it { should validate_uniqueness_of(:firebase_user_id) }
  end
end
