# frozen_string_literal: true

require "rails_helper"

describe "GET /api/v1/granulated_orders", type: :request do
  let(:user) { create(:user) }

  let!(:first_order) { create(:order, user: user, bought_at: Date.current - 2.months) }
  let!(:second_order) { create(:order, user: user, bought_at: Date.current - 1.year) }
  let!(:other_order) { create(:order, user: user, bought_at: Date.current) }

  let(:endpoint) { get api_v1_granulated_orders_path, params: params }

  before { sign_in(user) }

  before do
    endpoint
  end

  context "When missing params" do
    let(:params) { {} }

    it "fails" do
      expect(response).to have_http_status(:bad_request)
    end
  end

  context "When valid params" do
    let(:initial_date) { Date.current - 2.months }
    let(:final_date) { Date.current }
    let(:params) do
      {
        initial_date: initial_date,
        final_date: final_date
      }
    end

    it "succeeds" do
      expect(response).to have_http_status(:ok)
    end

    it "renders the index view" do
      expect(response).to render_template(:index)
    end

    context "by month" do
      let(:params) do
        {
          initial_date: initial_date,
          final_date: final_date,
          by: "month"
        }
      end

      it "succeeds" do
        expect(response).to have_http_status(:ok)
      end

      it "returns as many labels as number of order dates" do
        expect(data["labels"].count).to eq(3)
      end

      it "returns as many values as number of labels" do
        expect(data["values"].count).to eq(3)
      end
    end

    context "by month" do
      let(:params) do
        {
          initial_date: initial_date,
          final_date: final_date,
          by: "year"
        }
      end

      it "succeeds" do
        expect(response).to have_http_status(:ok)
      end

      it "returns as many labels as number of order dates by year" do
        expect(data["labels"].count).to eq(1)
      end

      it "returns as many values as number of labels" do
        expect(data["values"].count).to eq(1)
      end
    end

    context "by day or default" do
      let(:params) do
        {
          initial_date: initial_date,
          final_date: final_date,
          by: "day"
        }
      end

      it "succeeds" do
        expect(response).to have_http_status(:ok)
      end

      it "returns the quantity days between two dates on params for labels" do
        expect(data["labels"].count).to eq(63)
      end

      it "returns as many values as number of labels" do
        expect(data["values"].count).to eq(63)
      end
    end
  end
end
