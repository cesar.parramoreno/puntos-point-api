# frozen_string_literal: true

require "rails_helper"

describe "GET /api/v1/products", type: :request do
  let(:user) { create(:user) }

  let!(:first_product) { create(:product) }
  let!(:second_product) { create(:product) }
  let!(:other_product) { create(:product) }

  let(:endpoint) { get api_v1_products_path, params: params }

  before { sign_in(user) }

  context "When not filters used" do
    let(:params) { {} }

    before do
      endpoint
    end

    it "returns products ordered by most recent first" do
      expect(data.size).to be(3)
      expect(data[2]["id"]).to eq(first_product.id)
      expect(data[1]["id"]).to eq(second_product.id)
      expect(data[0]["id"]).to eq(other_product.id)
    end

    it "succeeds" do
      expect(response).to have_http_status(:ok)
    end
  end

  context "when filtering" do
    context "by category" do
      let(:product_category) { create(:product, :with_category) }
      let(:params) { { category: product_category.categories.first["name"] } }

      before do
        endpoint
      end

      it "succeeds" do
        expect(response).to have_http_status(:ok)
      end

      it "returns the product by category" do
        expect(data.size).to eq(1)
        expect(data[0]["id"]).to eq(product_category.id)
      end

      it "renders the index view" do
        expect(response).to render_template(:index)
      end
    end

    context "by most popular" do
      let!(:order) { create(:order, :with_10_products) }
      let(:new_order) { create(:order) }

      let(:params) { { more_popular: true } }

      before do
        3.times do
          order.products << Product.first
          order.save
        end

        endpoint
      end

      it "succeeds" do
        expect(response).to have_http_status(:ok)
      end

      it "return the 3 most popular products" do
        expect(data.size).to eq(3)
      end

      it "return first the product with more orders" do
        expect(data[0]["id"]).to eq(Product.first.id)
      end

      it "return the top total orders equal 3 sales for first product" do
        expect(data[0]["total_sales"]).to eq(3)
      end
    end
  end
end
