# frozen_string_literal: true

require "rails_helper"

describe "GET /api/v1/orders", type: :request do
  let(:user) { create(:user) }

  let!(:first_order) { create(:order, user: user) }
  let!(:second_order) { create(:order, user: user) }
  let!(:other_order) { create(:order, bought_at: "2022-01-01") }
  let!(:product_to_filter) { create(:product, :with_orders, name: "ProductName") }

  let(:endpoint) { get api_v1_orders_path, params: params }

  before { sign_in(user) }

  before do
    endpoint
  end

  context "when not filter used" do
    let(:params) { {} }

    it "returns orders ordered by most recent first" do
      expect(data.size).to be(4)
      expect(data[3]["id"]).to eq(first_order.id)
      expect(data[2]["id"]).to eq(second_order.id)
      expect(data[1]["id"]).to eq(other_order.id)
    end

    it "succeeds" do
      expect(response).to have_http_status(:ok)
    end

    it "renders the index view" do
      expect(response).to render_template(:index)
    end
  end

  context "when filtering" do
    context "by date" do
      let(:random_date) { "2022-01-01" }

      let(:params) { { date: random_date } }

      before do
        endpoint
      end

      it "succeeds" do
        expect(response).to have_http_status(:ok)
      end

      it "returns the orders into specific date" do
        expect(data.size).to eq(1)
        expect(data[0]["id"]).to eq(other_order.id)
      end
    end

    context "when filtering orders with specific name of product" do
      let(:params) { { product_name: "ProductName" } }

      it "succeeds" do
        expect(response).to have_http_status(:ok)
      end

      it "returns the order where product name match the search" do
        expect(data.size).to eq(1)
        expect(data[0]["products"].first["id"]).to eq(product_to_filter.id)
      end
    end
  end

  describe "rendering" do
    let!(:params) { {} }

    it "renders the index view" do
      endpoint
      expect(response).to render_template(:index)
    end
  end
end
