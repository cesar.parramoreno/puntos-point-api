# frozen_string_literal: true

namespace :db do
  desc "run custom test for puntos points"
  task populate: :environment do
    # Array Objects

    product_names = [
      "Parrillada",
      "Empanadas",
      "Honey",
      "Porter",
      "Kolsch",
      "Gaseosa",
      "Agua Mineral",
      "Agua Saborizada",
      "Copa de Vino",
      "Café",
      "Cortado",
      "Té",
      "Café con Leche"
    ]

    category_names = [
      "restaurant",
      "brewery",
      "coffee_shop"
    ]

    # Creating Categories
    category_names.count.times do |i|
      name = category_names[i]
  
      Category.create!(
        name: name
      )
    end

    customer_user = User.create!(
      firebase_user_id: "oxQVql6MxRXrLOlzo4csNWpa6X02",
      first_name: "Pepe",
      last_name: "Lopez",
      email: "pepe@gmail.com",
      provider: "password"
    )

    def product_price_for(product_name)
      case product_name
      when "Parrillada"
        2500
      when "Empanadas"
        80
      when "Honey"
        190
      when "Porter"
        200
      when "Kolsch"
        300
      when "Gaseosa"
        120
      when "Agua Mineral"
        50
      when "Agua Saborizada"
        60
      when "Copa de Vino"
        120
      when "Café"
        80
      when "Cortado"
        85
      when "Té"
        70
      when "Café con Leche"
        120
      end
    end

    # Products functions
    def product_description_for(product_name)
      case product_name
      when "Parrillada"
        "Parrillada del asador para 2 personas"
      when "Empanadas"
        "2 empanadas de carne para la entrada"
      when "Honey"
        "Nuestra Honey Beer recoge la historia que dio origen a “la luna de miel” y lo celebra con notas mento-ladas y frutales."
      when "Porter"
        "Maltas oscuras. Sabor y aroma penetrante y nocturno. Chocolate, azúcar negro y café."
      when "Kolsch"
        "Existen muchas cervezas doradas y refrescantes. Pero frutadas y con destellos finales de lúpulo, sólo hay un estilo: la Kölsch. "
      when "Gaseosa"
        "Gaseosas linea Coca Cola."
      when "Agua Mineral"
        "Agua mineral con o sin gas."
      when "Agua Saborizada"
        "Agua saborizada sabores varios: Citrus, Naranja, Manzana, UVA."
      when "Copa de Vino"
        "Copa de vino de la casa."
      when "Café"
        "Café chico, solo."
      when "Cortado"
        "Café chico, cortado."
      when "Té"
        "Selección de tés de la casa."
      when "Café con Leche"
        "Café chico, con leche."
      end
    end

    def product_category_for(product_name)
      case product_name
      when "Parrillada"
        Category.find_by(name: "restaurant")
      when "Empanadas"
        Category.find_by(name: "restaurant")
      when "Honey"
        Category.find_by(name: "brewery")
      when "Porter"
        Category.find_by(name: "brewery")
      when "Kolsch"
        Category.find_by(name: "brewery")
      when "Gaseosa"
        Category.find_by(name: "restaurant")
      when "Agua Mineral"
        Category.find_by(name: "restaurant")
      when "Agua Saborizada"
        Category.find_by(name: "restaurant")
      when "Copa de Vino"
        Category.find_by(name: "restaurant")
      when "Café"
        Category.find_by(name: "coffee_shop")
      when "Cortado"
        Category.find_by(name: "coffee_shop")
      when "Té"
        Category.find_by(name: "coffee_shop")
      when "Café con Leche"
        Category.find_by(name: "coffee_shop")
      end
    end

    # Products
    product_names.count.times do |i|
      product_name = product_names[i]
      product_description = product_description_for(product_name)
      product_price = product_price_for(product_name)
      category = product_category_for(product_name)

      product = Product.create!({
        name: product_name,
        description: product_description,
        price: product_price,
      })

      product.categories << category
      product.save
    end

    def product_order
      Product.where(id: rand(1..13))
    end

    # Orders
    100000.times do |i|
      products = product_order

      order = Order.create!({
        payment_method: ["debit", "credit", "cash"].sample,
        bought_at: Date.today - rand(1..12).months - rand(30).days,
        status: ["pending", "confirmed"].sample,
        user: customer_user
      })

      if order.payment_method.include?("cash")
        order.confirmed!
      end

      order.products << products
      order.save
    end
  end
end
